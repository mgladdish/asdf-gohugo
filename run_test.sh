#!/usr/bin/env bash

git clone https://github.com/asdf-vm/asdf.git $HOME/asdf
export PATH=$HOME/asdf/bin:$PATH

asdf plugin test gohugo . 'hugo version'
