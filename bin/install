#!/usr/bin/env bash

set -euo pipefail

[ -z "${ASDF_INSTALL_TYPE+x}" ] && echo "ASDF_INSTALL_TYPE is required" && exit 1
[ -z "${ASDF_INSTALL_VERSION+x}" ] && echo "ASDF_INSTALL_VERSION is required" && exit 1
[ -z "${ASDF_INSTALL_PATH+x}" ] && echo "ASDF_INSTALL_PATH is required" && exit 1

install() {
  local install_type=$1
  [ "$install_type" != "version" ] && echo "install type, $install_type, is not supported" && exit 1

  local version=$2
  local raw_version=${version#"extended_"}
  local install_path=$3

  local platform
  [ "Linux" = "$(uname)" ] && platform="Linux" || platform="macOS"
  local arch
  [ "x86_64" = "$(uname -m)" ] && arch="64bit" || arch="32bit"

  if [ -z "${TMPDIR+x}" ]; then
    local tmp_download_dir=$(mktemp -d)
  else
    local tmp_download_dir=$TMPDIR
  fi

  local download_url="https://github.com/gohugoio/hugo/releases/download/v${raw_version}/hugo_${version}_${platform}-${arch}.tar.gz"

  curl -sL ${download_url} -o ${tmp_download_dir}/hugo_${version}_${platform}-${arch}.tar.gz || exit 2
  tar xf ${tmp_download_dir}/hugo_${version}_${platform}-${arch}.tar.gz -C ${install_path} || exit 3

  mkdir ${install_path}/bin
  mv ${install_path}/hugo ${install_path}/bin/
}

install "$ASDF_INSTALL_TYPE" "$ASDF_INSTALL_VERSION" "$ASDF_INSTALL_PATH"
