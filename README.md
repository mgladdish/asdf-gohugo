# asdf-gohugo

[gohugo.io](https://gohugo.io) plugin for the [asdf version manager](https://github.com/asdf-vm/asdf).

Lists **two** variants for each version, the regular build and the _extended_ build which also includes Scss/SASS support.
Unfortunately the extended builds are only available for a subset of OSes and architectures so if installing an extended 
version fails you may have more luck by trying the "regular" version instead.

## Dependencies

Requires:

* curl
* jq
* tar
* tr
